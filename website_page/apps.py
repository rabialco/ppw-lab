from django.apps import AppConfig


class WebsitePageConfig(AppConfig):
    name = 'website_page'
