from django.urls import path
from .views import home, profile, experiences, contact, comingsoon

urlpatterns = [ 
    path('', home, name= 'home'),
    path('profile/', profile, name='profile'),
    path('experiences/', experiences, name='experiences'),
    path('contact/', contact, name='contact'),
    path('comingsoon/', comingsoon, name='comingsoon')
]