from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, "landing-page.html")

def profile(request):
    return render(request, "profile-page-edit.html")

def experiences(request):
    return render(request, "experiences-page-edit.html")

def contact(request):
    return render(request, "contact-page.html")

def comingsoon(request):
    return render(request, "comingsoon-page.html")