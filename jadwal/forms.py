from django import forms

class formulir(forms.Form):
    activity_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Your activity name',
        'type' : 'text',
        'required' : True
    }))

    day = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Day',
        'type' : 'text',
        'required' : True
    }))

    date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Date',
        'type' : 'date',
        'required' : True
    }))

    time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Time',
        'type' : 'time',
        'required' : True
    }))

    place = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Place',
        'type' : 'text',
        'required' : True
    }))

    category = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' :  'Category',
        'type' : 'text',
        'required' : True
    }))