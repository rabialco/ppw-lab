from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.TextField(max_length=10, blank = False)
    hari = models.CharField(max_length=10, blank = False)
    tanggal = models.DateField(max_length=10, blank = False)
    waktu = models.TimeField(max_length=10, blank = False)
    tempat = models.TextField(max_length=10, blank = False)
    kategori = models.CharField(max_length=10, blank = False)

    def __str__(self):
        return self.nama_kegiatan