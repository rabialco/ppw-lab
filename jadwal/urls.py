from django.urls import path
from .views import schedule, delete

urlpatterns = [ 
    path('', schedule, name= 'home'),
    path('<int:pk>', delete, name='remove')
]