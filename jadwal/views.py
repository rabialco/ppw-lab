from django.shortcuts import render, redirect
from .forms import formulir
from .models import Jadwal

# Create your views here.
def schedule(request):
    if request.method == 'POST':
        formInput = formulir(request.POST)
        if (formInput.is_valid()):
            jadwalInput = Jadwal()
            jadwalInput.nama_kegiatan = formInput.cleaned_data['activity_name']
            jadwalInput.hari = formInput.cleaned_data['day']
            jadwalInput.tanggal = formInput.cleaned_data['date']
            jadwalInput.waktu = formInput.cleaned_data['time']
            jadwalInput.tempat = formInput.cleaned_data['place']
            jadwalInput.kategori = formInput.cleaned_data['category']
            jadwalInput.save()
        return redirect('/jadwal')
    else :
        formInput = formulir()
        jadwalInput = Jadwal.objects.all()
        response = {
            'formulir' : formInput, 
            'jadwal' : jadwalInput
        }
        return render(request, 'schedule/schedule-page.html', response)


def delete(request, pk):
    if request.method == 'POST':
        formInput = formulir(request.POST)
        if (formInput.is_valid()):
            jadwalInput = Jadwal()
            jadwalInput.nama_kegiatan = formInput.cleaned_data['activity_name']
            jadwalInput.hari = formInput.cleaned_data['day']
            jadwalInput.tanggal = formInput.cleaned_data['date']
            jadwalInput.waktu = formInput.cleaned_data['time']
            jadwalInput.tempat = formInput.cleaned_data['place']
            jadwalInput.kategori = formInput.cleaned_data['category']
            jadwalInput.save()
        return redirect('/jadwal')
    else :
        Jadwal.objects.filter(pk=pk).delete()
        formInput = formulir()
        jadwalInput = Jadwal.objects.all()
        response = {
            'formulir' : formInput, 
            'jadwal' : jadwalInput
        }
        return render(request, 'schedule/schedule-page.html', response)



    # Jadwal.objects.filter(id=key).delete()
    # return redirect('/jadwal')
